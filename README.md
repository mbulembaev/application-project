# Тестовое приложение
Проверка заявки кредитной карты

## Настройка
Нужно создать файл `src/project/.env`. Пример содержания ниже:
```
DEBUG=on
SECRET_KEY=your-secret-key
DATABASE_URL=sqlite:///my-local-sqlite.db
```

## Установка
Нужен pip-tools. Для установки всех зависимостей:
```
$ python -m venv venv
$ source venv/bin/activate
(venv)$ python -m pip install pip-tools
(venv)$ make
```

Testing:
```
# run unit tests
$ cd src
$ pytest
```

Development server:
```
$ ./manage.py runserver
```

Start Celery worker:
```
$ celery -A project beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler
```