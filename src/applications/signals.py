import django.dispatch

# Signal to notify about application's state change
# - old_state
# - new_state
# - instance
# - date
state_changed = django.dispatch.Signal()
