from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _
from applications.models import Application, ApplicationState
from applications.permissions import IsOwnerOnly
from applications.serializers import ApplicationSerializer, ApplicationListSerializer


class ApplicationViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated,
                          IsOwnerOnly]
    serializer_classes = {
        'list': ApplicationListSerializer,
        'retrieve': ApplicationListSerializer,
    }
    default_serializer_class = ApplicationSerializer

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_queryset(self, *args, **kwargs):
        return Application.objects.filter(author=self.request.user).active()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        if instance.state in [ApplicationState.new, ApplicationState.rework]:
            serializer = self.get_serializer(instance, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            return Response(serializer.data)
        return Response({"status": _("Редактирование доступно только для статуса new или rework")},
                        status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)
