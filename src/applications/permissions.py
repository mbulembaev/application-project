from rest_framework import permissions


class IsOwnerOnly(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to update, delete and view it.
    """

    def has_object_permission(self, request, view, obj):
        # Instance must have an attribute named `author`.
        return obj.author == request.user
