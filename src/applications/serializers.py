from rest_framework import serializers
from applications.models import Application
from django.utils.translation import gettext_lazy as _


class ApplicationSerializer(serializers.ModelSerializer):

    def validate_card_number(self, value):
        """
        Check that the user doesn't have an application with provided card number.
        """
        user = self.context['request'].user
        applications = Application.objects.filter(card_number=value, author=user).active()

        if applications.exists():
            raise serializers.ValidationError(_("У вас уже есть заявка с таким номером карты"))

        return value

    class Meta:
        model = Application
        fields = ['id', 'owner_name', 'card_number', 'file']


class ApplicationListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Application
        fields = ['id', 'owner_name', 'card_number', 'file', 'state']
