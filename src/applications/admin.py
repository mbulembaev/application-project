from django.contrib import admin
from django.contrib.admin import ModelAdmin

from applications import models


@admin.register(models.Application)
class ApplicationAdmin(ModelAdmin):
    list_display = ('id', 'state', 'author', 'editor', 'deleted')
    list_filter = ('state',)
    search_fields = ('owner_name', 'card_number')
    list_select_related = ['author', 'editor']
    readonly_fields = ('created', 'modified')

    status_fields = ['state']
    application_fields = [
        'author', 'owner_name', 'card_number', 'file'
    ]
    service_fields = ['note', 'created', 'modified', 'editor', 'deleted']

    fieldsets = (
        ('Application Details', {
            'fields': application_fields
        }),
        ('Status', {
            'fields': status_fields
        }),
        ('Service Fields', {
            'fields': service_fields,
        }),
    )


@admin.register(models.ApplicationStateHistory)
class ApplicationStateHistoryAdmin(ModelAdmin):
    list_display = ('id', 'application', 'old_state', 'new_state', 'date')
    list_select_related = ['application', ]
    readonly_fields = ['application', 'old_state', 'new_state', 'date']
