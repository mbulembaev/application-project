from celery import shared_task
from datetime import datetime
from dateutil.relativedelta import relativedelta

from applications.models import Application, ApplicationState


@shared_task
def change_application_state_from_verified_to_rework():
    months = 6
    now = datetime.now()
    from_datetime = now - relativedelta(months=months)

    applications = Application.objects.filter(state=ApplicationState.verified,
                                              modified__lte=from_datetime)

    for application in applications:
        application.mark_as_reworked()
