from django.core.validators import FileExtensionValidator
from django.db import models
from behaviors.behaviors import (
    Authored,
    Editored,
    StoreDeleted,
    Timestamped,
)
from django.utils.translation import gettext_lazy as _
from applications.managers import ApplicationQuerySet
from applications.signals import state_changed
from applications.state import TextStates


class ApplicationState(TextStates):
    new = 'new', _('Новая')
    verified = 'verified', _('Проверена')
    reject = 'reject', _('Заблокирована')
    rework = 'rework', _('Требует исправления данных пользователем')


class Application(Authored, Editored, StoreDeleted, Timestamped):
    owner_name = models.CharField(_('Имя на карте'), max_length=200)
    card_number = models.CharField(_('Номер карты'), max_length=20)
    file = models.FileField(_('Файл'),
                            validators=[FileExtensionValidator(allowed_extensions=['jpg', 'png', 'pdf'])])
    state = models.CharField(max_length=50, choices=ApplicationState.choices,
                             default=ApplicationState.new)
    note = models.TextField(_('Примечание'), blank=True)

    objects = ApplicationQuerySet.as_manager()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._initial_state = self.state

    def __str__(self):
        return str(self.id)

    def mark_as_reworked(self):
        self.state = ApplicationState.rework
        self.save()

    class Meta:
        verbose_name = _('Заявка')
        verbose_name_plural = _('Заявки')
        unique_together = ('card_number', 'author')

    def save(self, *args, **kwargs):
        updated = bool(self.id)
        applications = Application.objects.filter(card_number=self.card_number).active()

        if applications.exists():
            self.note = _(f'Текущий номер карты указан у следующих заявок {[a.id for a in applications]}')

        super().save(*args, **kwargs)

        if updated and self._initial_state != self.state:
            state_changed.send(
                sender=Application,
                old_state=self._initial_state,
                new_state=self.state,
                instance=self,
            )


class ApplicationStateHistory(models.Model):
    application = models.ForeignKey('applications.Application', on_delete=models.CASCADE)
    old_state = models.CharField(max_length=50, choices=ApplicationState.choices)
    new_state = models.CharField(max_length=50, choices=ApplicationState.choices)
    date = models.DateTimeField(auto_now=True)
