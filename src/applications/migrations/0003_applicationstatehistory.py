# Generated by Django 3.1.7 on 2021-03-21 09:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('applications', '0002_auto_20210321_0939'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApplicationStateHistory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('old_state', models.CharField(choices=[('new', 'Новая'), ('verified', 'Проверена'), ('reject', 'Заблокирована'), ('rework', 'Требует исправления данных пользователем')], max_length=50)),
                ('new_state', models.CharField(choices=[('new', 'Новая'), ('verified', 'Проверена'), ('reject', 'Заблокирована'), ('rework', 'Требует исправления данных пользователем')], max_length=50)),
                ('date', models.DateTimeField(blank=True, null=True)),
                ('application', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='applications.application')),
            ],
        ),
    ]
