# Generated by Django 3.1.7 on 2021-03-21 09:39

from django.conf import settings
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('applications', '0001_initial'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='application',
            unique_together={('card_number', 'author')},
        ),
    ]
