from abc import abstractmethod
from types import SimpleNamespace
from typing import List, Dict
from django.db.models import TextChoices


class TextStates(TextChoices):
    @classmethod
    def next_states(cls, state: str, include_self=False) -> List[str]:
        states = SimpleNamespace()
        for k, v in cls.choices:
            setattr(states, k, k)

        transitions = cls.get_transitions(states)
        ret = transitions.get(state, [])
        if include_self:
            ret.append(getattr(cls, state))

        return ret

    @classmethod
    @abstractmethod
    def get_transitions(cls, states) -> Dict[str, List[str]]:
        """Implement in subclass"""
