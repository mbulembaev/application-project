from applications.models import ApplicationStateHistory
from applications.signals import state_changed


def on_application_state_change(sender, **kwargs):
    application = kwargs['instance']
    old_state = kwargs['old_state']
    new_state = kwargs['new_state']

    ApplicationStateHistory.objects.create(
        application=application,
        old_state=old_state,
        new_state=new_state,
    )


state_changed.connect(on_application_state_change, dispatch_uid='log_state')
