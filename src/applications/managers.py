from django.db import models


class ApplicationQuerySet(models.QuerySet):
    def active(self):
        return self.filter(deleted__isnull=True)
